import unittest
import app

class test_homework(unittest.TestCase):
    def test_add(self):
        instance = app.home_work()
        result = instance.add(5,6)
        self.assertEqual(int(result),11)

    def test_sub(self):
        instance = app.home_work()
        result = instance.sub(5,12)
        self.assertEqual(int(result),-7)        

if __name__ == "__main__":
    unittest.main()
