from flask import Flask, request
app = Flask(__name__)

class home_work():
   def print_name(self,name):
      return f"hello bob from {name}"

   def add(self,a,b):
      return str(a + b)

   def sub(self,a,b):
      return str(a - b)



hw = home_work()

@app.route('/',methods=["GET"])
def print_name():
   return hw.print_name(request.args.get('name'))

@app.route('/add', methods=["GET"])
def add():
   a = int(request.args.get('a'))
   b = int(request.args.get('b'))
   return hw.add(a,b)



@app.route('/sub', methods=["GET"])
def sub():
   a = int(request.args.get('a'))
   b = int(request.args.get('b'))
   return hw.sub(a,b)



if __name__ == '__main__':

   app.run(host='0.0.0.0',port=8011)

